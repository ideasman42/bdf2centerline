#!/usr/bin/env python3
# Apache 2.0 licensed

# Warning, code quality here isn't great,
# but works OK to extract segments from a bitmap font.

# python bdf2centerline.py --output=SomeName /src/triskweline-code-font/bdf/TriskwelineNormal-12.bdf

import sys

def parse_bdf(f, MAX_CHARS=256):
    lines = [l.strip().upper().split() for l in f.readlines()]

    is_bitmap = False
    dummy = {"BITMAP": []}
    char_data = [dummy.copy() for i in range(MAX_CHARS)]
    context_bitmap = []

    for l in lines:
        if l[0] == "ENCODING":
            enc = int(l[1])
        elif l[0] == "BBX":
            bbx = [int(c) for c in l[1:]]
        elif l[0] == "DWIDTH":
            dwidth = int(l[1])
        elif l[0] == "BITMAP":
            is_bitmap = True
        elif l[0] == "ENDCHAR":
            if enc < MAX_CHARS:
                char_data[enc]["BBX"] = bbx
                char_data[enc]["DWIDTH"] = dwidth
                char_data[enc]["BITMAP"] = context_bitmap

            context_bitmap = []
            enc = bbx = None
            is_bitmap = False
        else:
            # None of the above, Ok, were reading a bitmap
            if is_bitmap and enc < MAX_CHARS:
                context_bitmap.append(int(l[0], 16))

    return char_data


def show(bdf_dict, font_name, origfilename, MAX_CHARS=256):

    # first get a global width/height, also set the offsets
    xmin = ymin = 10000000
    xmax = ymax = -10000000

    bitmap_offsets = [-1] * MAX_CHARS
    bitmap_tot = 0
    for i, c in enumerate(bdf_dict):
        bbx = c.get("BBX")
        if bbx is not None:
            xsize = bbx[0]
            ysize = bbx[1]
            xofs = bbx[2]
            yofs = bbx[3]
            xmax = max(xofs + xsize, xmax)
            ymax = max(yofs + ysize, ymax)
            xmin = min(xofs, xmin)
            ymin = min(yofs, ymin)

            bitmap_offsets[i] = bitmap_tot
            bitmap_tot += len(c['BITMAP'])

        c['BITMAP'].reverse()

    def glyph_to_verts(c):
        bmp = c["BITMAP"]
        if not bmp:
            return []

        width = c["DWIDTH"]
        x_ofs, y_ofs = c.get("BBX")[2:4]
        x = 0
        y = 0
        verts = []
        for cdata in bmp:
            for i in range(8):
                if cdata & (1 << (7 - i)):
                    verts.append((x + x_ofs, y + y_ofs))
                x += 1
                if x >= width:
                    y += 1
                    x = 0
        return verts

    def edges_from_verts(verts):
        # dict of (x, y) tuples
        verts_to_index = {v: i for (i, v) in enumerate(verts)}
        index_to_verts = {i: v for (i, v) in enumerate(verts)}
        verts_index_used = set()
        edges = []

        OPTIMIZE = True
        if OPTIMIZE:
            # span larger segments
            edges_redundant = set()

        for i_p, xy_p in enumerate(verts):
            (xp, yp) = xy_p
            xn = xp + 1
            yn = yp + 1
            xy_n = (xn, yn)
            other = (
                (+0, +1), # |
                (+1, +0), # -
                (+1, +1), # /
                (-1, +1), # \
            )

            for xo, yo in other:
                xn = xp + xo
                yn = yp + yo
                xy_n = xn, yn
                i_n = verts_to_index.get(xy_n)
                if i_n is not None:

                    # ignore diagonals attached to vert/hoz
                    if xo and yo:
                        if (xp + xo, yp) in verts_to_index or (xp, yp + yo) in verts_to_index:
                            continue

                    verts_index_used.add(i_p)
                    verts_index_used.add(i_n)
                    if not OPTIMIZE:
                        # add every edge
                        edges.append((i_p, i_n))
                    else:
                        if (i_p, i_n) in edges_redundant:
                            # we've handled this segment previously
                            pass
                        else:
                            # walk the direction until we stop
                            i_s_prev = i_n
                            while True:
                                # s for step
                                xy_s = (xn + xo, yn + yo)
                                i_s = verts_to_index.get(xy_s)
                                is_break = False
                                if i_s is None:
                                    is_break = True
                                elif xo and yo:
                                    if (xn + xo, yn) in verts_to_index or (xn, yn + yo) in verts_to_index:
                                        is_break = True

                                if is_break:
                                    edges.append((i_p, i_s_prev))
                                    break

                                # don't add this again!
                                edges_redundant.add((i_s_prev, i_s))
                                verts_index_used.add(i_s)

                                xn, yn = xy_s
                                i_s_prev = i_s


                        # edges.append((i_p, i_n))
                        # keep scanning

        for i_p, xy_p in enumerate(verts):
            if i_p not in verts_index_used:
                # single point edge
                edges.append((i_p, i_p))

        return edges

    if False:
        # OBJ files, for quick testing
        for c_index, c in enumerate(bdf_dict):
            verts = glyph_to_verts(c)
            edges = edges_from_verts(verts)
            if verts:
                with open(f"/tmp/{c_index}.obj", 'w') as f:
                    fw = f.write
                    # print(verts)
                    # print(edges)

                    for v in verts:
                        fw(f"v {v[0]} {v[1]} 0\n")
                    for e in edges:
                        i0 = e[0] + 1
                        i1 = e[1] + 1
                        fw(f"f {i0} {i1}\n")

    if True:
        data = []
        edges_total = 0
        for c_index, c in enumerate(bdf_dict):
            verts = glyph_to_verts(c)
            edges = edges_from_verts(verts)
            data.append((c_index, c, verts, edges))
            edges_total += len(edges)

        # Rust data structure
        with open(f"{font_name}.rs", 'w') as f:
            chars_total = 0
            fw = f.write
            fw(f"const DATA: [[[i8; 2]; 2]; {edges_total}] = [\n")
            for c_index, c, verts, edges in data:
                if verts:
                    fw(f"    // {c_index}\n")
                    for e in edges:
                        (va0, va1) = verts[e[0]]
                        (vb0, vb1) = verts[e[1]]
                        fw(f"    [[{va0}, {va1}], [{vb0}, {vb1}]],\n")
                    chars_total += 1
            fw("];\n\n")

            chars_step = 0
            edges_step = 0
            fw(f"const CHARS: [::std::ops::Range<usize>; {255}] = [\n")
            for c_index, c, verts, edges in data:
                if verts:
                    chars_step += 1
                    edges_step_next = edges_step + len(edges)
                    fw(f"    {edges_step}..{edges_step_next},\n")
                    edges_step = edges_step_next
                else:
                    fw(f"    0..0,\n")
            fw("];\n")
        print(f"Written: '{font_name}.rs'")

    if 0:
        f = sys.stdout
        for c_index, c in enumerate(bdf_dict):
            bitmap = [[]]
            j = 0
            for cdata in c["BITMAP"]:
                for i in range(8):
                    if cdata & (1 << (7 - i)):
                        bitmap[-1].append("#")
                    else:
                        bitmap[-1].append(" ")
                    j += 1
                    if j >= c["DWIDTH"]:
                        bitmap.append([])
                        j = 0
            bitmap.reverse()
            f.write(f'# {c_index} {c.get("BBX")}\n')
            for l in bitmap:
                f.write("".join(l))
                f.write("\n")
            f.write("\n")


def main():
    # replace "[--output=foo]" with "[--output] [foo]"
    args = []
    for arg in sys.argv:
        for a in arg.replace("=", " ").split():
            args.append(a)

    name = "untitled"
    done_anything = False
    for i, arg in enumerate(args):
        if arg == '--output':
            if i == len(args) - 1:
                print("no arg given for --output, aborting")
                return
            else:
                name = args[i + 1]

        elif arg.lower().endswith('.bdf'):
            try:
                f = open(arg)
            except Exception as e:
                print("Error %r" % e)

            bdf_dict = parse_bdf(f)
            show(bdf_dict, name, arg)
            done_anything = True

    if not done_anything:
        print(HELP_TXT)
        print("...nothing to do")

if __name__ == "__main__":
    main()
