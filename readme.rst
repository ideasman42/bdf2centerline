
BDF to CenterLine
=================

Quick and dirty utility to convert BDF (bitmap fonts) to center-lines,
represented as segments, extracted from single pixel-width bitmap fonts.


Usage
-----

.. code-block:: sh

   python3 bdf2centerline.py --output=some_file.rs /path/to/font.bdf


TODO
----

Some possible improvements:

- Support line gradients besides straight lines and diagonals.
- Add options to output different formats
  (currently hard-coded to write Rust formatted data).
- Extract center-lines for multiple pixel width lines.

Note that currently this works for my own needs, just listing for completeness.

